#include "Coverage.h"

#include <Wt/WItemDelegate>
#include <Wt/WStandardItem>
#include <Wt/WStandardItemModel>
#include <Wt/WEnvironment>
#include <Wt/WVBoxLayout>
#include <Wt/WProgressBar>
#include <Wt/WContainerWidget>
#include <Wt/WTreeView>
#include <boost/lexical_cast.hpp>
#include <boost/format.hpp>
#include <boost/any.hpp>
#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/foreach.hpp>
#include <QFile>
#include <QXmlStreamReader>
#include <algorithm>
#include "CodeBrowserDlg.h"

using namespace Wt;

Wt::WStandardItemModel *Coverage::folderModel = NULL;
std::string gSourcePath;
std::string gCovXmlPath;
std::map<std::string, std::vector < std::string > > gFilterMap;


class MyProgressBar : public WProgressBar
{
public:
	virtual WString text() const
	{
		return m_text;
	}

	void setText(const WString &text)
	{
		m_text = text;
	}

	virtual ~MyProgressBar() {}

private:

	WString m_text;

};


class ProgressBarDelegate : public WItemDelegate
{
	WWidget *update(WWidget *widget, const WModelIndex& index, WFlags<ViewItemRenderFlag> flags)
	{
        Q_UNUSED(widget);
        Q_UNUSED(flags);

		if (index.internalPointer() == Coverage::folderModel->invisibleRootItem())
		{
			return new WText("N/A");
		}

		int cov = boost::any_cast<int>(Coverage::folderModel->data(index, UserRole));
		int total = boost::any_cast<int>(Coverage::folderModel->data(index, UserRole + 1));
		double value = 100.0 * cov / total;

		MyProgressBar *progressBar = new MyProgressBar();
		
		if (total == 0)
		{
            value = 0.0;
		}

		progressBar->setValue(value);

		WString text = WString("{1} / {2} - ({3} %)").arg(cov).arg(total).arg(static_cast<int>(value));
		progressBar->setText(text);

		return progressBar;
	}
};

WApplication *createApplication(const WEnvironment& env)
{
	WApplication *app = new Coverage(env);
	app->setTwoPhaseRenderingThreshold(0);
	app->setTitle("MCO Coverage");
	app->useStyleSheet("styles.css");
	app->refresh();
	return app;
}

void Coverage::showCode(const WModelIndex &index)
{
	if (folderModel->data(index, UserRole + 1).empty() || folderModel->data(index, UserRole + 1).empty())
	{
		return;
	}

	std::string fileName = boost::any_cast<std::string>(folderModel->data(index, UserRole));
	std::string probeString = boost::any_cast<std::string>(folderModel->data(index, UserRole + 1));

	if (boost::filesystem::is_regular_file(fileName) && !probeString.empty())
	{
		m_codeDlg->prepare(fileName, probeString);
		m_codeDlg->show();
	}
}


Coverage::Coverage(const WEnvironment &env) : WApplication(env)
{
	setCssTheme("polished");
	createUI();
	m_codeDlg = new CodeBrowserDlg(this);
	m_folderView->doubleClicked().connect(this, &Coverage::showCode);
}

Coverage::~Coverage()
{

}

void Coverage::createUI() {
	WContainerWidget *w = root();
	w->setStyleClass("maindiv");
	WVBoxLayout *layout = new WVBoxLayout();
	layout->addWidget(folderView());
	w->setLayout(layout);
}


WTreeView *Coverage::folderView() {
	WTreeView *treeView = new WTreeView();

	treeView->setModel(folderModel);
	treeView->resize(1000, WLength::Auto);
	treeView->setSelectionMode(SingleSelection);
	treeView->setItemDelegateForColumn(1, new ProgressBarDelegate());
	treeView->setItemDelegateForColumn(2, new ProgressBarDelegate());
	treeView->setAlternatingRowColors(true);
	m_folderView = treeView;

	return treeView;
}

std::vector<WStandardItem *> createRow(const std::string &name, int fn_cov, int fn_total, int d_cov, int d_total, const std::string &fileName)
{
	std::vector<WStandardItem *> row;
	WStandardItem *col1 = new WStandardItem(name);
	WStandardItem *col2 = new WStandardItem(name);
	WStandardItem *col3 = new WStandardItem(name);

	col2->setData(0, UserRole);
	col2->setData(0, UserRole + 1);
	col3->setData(0, UserRole);
	col3->setData(0, UserRole + 1);

	if (fn_total)
	{
		col2->setData(fn_cov, UserRole);
		col2->setData(fn_total, UserRole + 1);
	}
	if (d_total)
	{
		col3->setData(d_cov, UserRole);
		col3->setData(d_total, UserRole + 1);
	}

	col1->setData(fileName, UserRole);

	row.push_back(col1);
	row.push_back(col2);
	row.push_back(col3);

	return row;
}

std::string removeFileExtension(const std::string &fileName)
{
    size_t start = fileName.find_last_of(boost::filesystem::path::preferred_separator) + 1;
    size_t end = fileName.find_first_of('.');
    std::string execName = fileName.substr(start, end - start);

    return execName;
}

std::vector<WStandardItem *> createExecutableItem(const std::string &fileName)
{
    std::string execName = removeFileExtension(fileName);

	std::vector<WStandardItem *> row = createRow(execName, 0, 0, 0, 0, "");
	row.front()->setIcon("icons/app.gif");
	return row;
}


void SetHeaderData()
{
    Coverage::folderModel = new WStandardItemModel(0, 3);
    Coverage::folderModel->setHeaderData(0, Horizontal, std::string("Object"));
    Coverage::folderModel->setHeaderData(1, Horizontal, std::string("Function Coverage"));
    Coverage::folderModel->setHeaderData(2, Horizontal, std::string("Branch Coverage"));
}

WStandardItem * CreateRootItem(const std::string& fileName)
{
    WStandardItem *current = Coverage::folderModel->invisibleRootItem();
    std::vector<WStandardItem *> row = createExecutableItem(fileName);
    current->appendRow(row);
    current = row.front();

    return current;
}

void appendProbe(std::string &probeString, QXmlStreamReader &xml)
{
    std::string lineNr = xml.attributes().at(0).value().toString().toStdString();
    std::string type = xml.attributes().at(1).value().toString().toStdString();
    std::string event = xml.attributes().at(2).value().toString().toStdString();
    probeString += lineNr + ";" + type + ";" + event + ";";
}

void insertTreeElement(boost::filesystem::path &path, QXmlStreamReader &xml, WStandardItem *&current)
{
    int attributeOffset = 0;
    if (xml.name() == "src")
    {
        attributeOffset = 1;
    }

    std::string name = xml.attributes().at(0).value().toString().toStdString();
    int fn_cov = xml.attributes().at(attributeOffset + 1).value().toInt();
    int fn_total = xml.attributes().at(attributeOffset + 2).value().toInt();
    int d_cov = xml.attributes().at(attributeOffset + 5).value().toInt();
    int d_total = xml.attributes().at(attributeOffset + 6).value().toInt();

    std::vector<WStandardItem *> row = createRow(name, fn_cov, fn_total, d_cov, d_total, path.string());
    current->appendRow(row);
    current = row.front();

    if (xml.name() == "folder")
    {
        current->setIcon("icons/folder.gif");
    }

    if (xml.name() == "src")
    {
        current->setIcon("icons/file.gif");
    }
}

bool isTreeElement(const QXmlStreamReader &xml)
{
    return xml.name() == "folder" || xml.name() == "src" || xml.name() == "fn";
}

bool isInSearchPath(const boost::filesystem::path &path, const std::vector<std::string> &filters)
{
    if (filters.empty())
    {
        return false;
    }

    std::string pathStr = path.string();
    pathStr =  pathStr.substr(gSourcePath.length() + 1, pathStr.length());
    std::vector < std::string > pathTokens;
    boost::split(pathTokens, pathStr, boost::is_any_of("\\/"));

    int numFilters = filters.size();
    int matchedFilters = numFilters;

    BOOST_FOREACH(std::string filter, filters)
    {
        std::vector < std::string > filterTokens;
        boost::split(filterTokens, filter, boost::is_any_of("\\/"));

        for (size_t i = 0; i  < std::min(pathTokens.size(), filterTokens.size()); i++)
        {
             if (pathTokens.at(i) != filterTokens.at(i))
             {
                 matchedFilters--;
                 break;
             }
        }
    }

    return matchedFilters > 0;
}

void parseCoverageFile(const std::string & fileName)
{
    std::cout << "start parsing " << fileName << std::endl;
	boost::filesystem::path path(gSourcePath);

	if (!Coverage::folderModel)
	{
        SetHeaderData();
	}

    WStandardItem *current = CreateRootItem(fileName);
    WStandardItem *rootItem = current;

    QFile file(QString(fileName.c_str()));
    file.open(QIODevice::ReadOnly | QIODevice::Text);
    QXmlStreamReader xml(&file);

    std::string baseName = removeFileExtension(fileName);
    std::vector<std::string> filters = gFilterMap[baseName];

    std::string probeString;
    bool inSearchPath = false;

    while(!xml.atEnd() && !xml.hasError())
	{
        if (xml.isStartElement())
        {            
            if (isTreeElement(xml))
            {
                std::string name = xml.attributes().at(0).value().toString().toStdString();
                if (xml.name() == "folder" || xml.name() == "src")
                {
                    path /= name;
                }

                inSearchPath = isInSearchPath(path, filters);

                if (inSearchPath)
                {
                    insertTreeElement(path, xml, current);
                }
            }

            if (xml.name() == "probe")
            {
                appendProbe(probeString, xml);
            }
        }

        if (xml.isEndElement())
        {
            if (xml.name() == "src")
            {
                current->setData(probeString, UserRole + 1);
                probeString.clear();
            }

            if (isTreeElement(xml))
			{
                if (inSearchPath)
                {
                    current = current->parent();
                }
                if (!current)
                {
                    current = rootItem;
                }
			}

            if (xml.name() == "folder" || xml.name() == "src")
			{
                path = path.parent_path();
			}
		}

        xml.readNext();
	}

    std::cout << "end parsing " << fileName << std::endl;
}


void processFile(boost::filesystem::path path)
{
	std::wstring ws(path.c_str());
	std::string fileName(ws.begin(), ws.end());
	std::string extension(".xml");

	if (fileName.compare(fileName.length() - extension.length(), extension.length(), extension) == 0)
	{
		parseCoverageFile(fileName);
	}
}

void readConfig()
{
	std::string line;
	std::ifstream file("config.txt");
	if (file.is_open())
	{
		if (std::getline(file, line))
		{
			gCovXmlPath = line;
		}

		if (std::getline(file, line))
		{
			gSourcePath = line;
		}

        while (std::getline(file, line))
        {
            std::vector < std::string > tokens;
            boost::split(tokens, line, boost::is_any_of(";"));

            if (tokens.size())
            {
                std::string key = tokens.front();
                tokens.erase(tokens.begin(), tokens.begin() + 1);
                gFilterMap.insert(std::make_pair(key, tokens));
            }
        }

		file.close();
	}

}


int main(int argc, char **argv)
{
	readConfig();
	boost::filesystem::path path(gCovXmlPath);
	std::for_each(boost::filesystem::directory_iterator(path), boost::filesystem::directory_iterator(), processFile);

    return WRun(argc, argv, &createApplication);
}

