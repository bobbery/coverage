#ifndef COVERAGE_H
#define COVERAGE_H

#include <Wt/WApplication>


namespace Wt {
	class WStandardItemModel;
	class WEnvironment;
	class WTreeView;
	class WModelIndex;
}

class CodeBrowserDlg;

class Coverage : public Wt::WApplication
{
public:

	Coverage(const Wt::WEnvironment &env);

	virtual ~Coverage();

	static Wt::WStandardItemModel *folderModel;

private:

	void showCode(const Wt::WModelIndex &index);

	Wt::WTreeView *m_folderView;
	CodeBrowserDlg *m_codeDlg;
	void createUI();
	Wt::WTreeView *folderView();

};

#endif
