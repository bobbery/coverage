#ifndef CODE_BROWSER_DLG_H
#define CODE_BROWSER_DLG_H

#include <Wt/WDialog>


namespace Wt {
	class WStandardItemModel;
	class WTableView;
}

class CodeBrowserDlg : public Wt::WDialog
{
public:
	CodeBrowserDlg(WObject *parent = 0);

	void prepare(const std::string & fileName, const std::string & probeString);

	virtual ~CodeBrowserDlg();


private:
	Wt::WTableView *m_tableView;
	Wt::WStandardItemModel *m_lineModel;
};

#endif