QT -= gui
QT += core

TARGET = coverage
CONFIG   += console

TEMPLATE = app
TARGET = coverage

INCLUDEPATH += . \

win32 {
    INCLUDEPATH += C:\libraries\include
    LIBS += -LC:\libraries\lib
}

LIBS += \
    -lwt \
    -lwthttp \
    -lboost_filesystem \
    -lboost_system \

# Input
HEADERS += CodeBrowserDlg.h \
           Coverage.h \

SOURCES += CodeBrowserDlg.cpp Coverage.cpp

