#include "CodeBrowserDlg.h"

#include <Wt/WContainerWidget>
#include <Wt/WVBoxLayout>
#include <Wt/WPushButton>
#include <Wt/WTableView>
#include <Wt/WStandardItem>
#include <Wt/WStandardItemModel>
#include <Wt/WCssDecorationStyle>
#include <fstream>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>

using namespace Wt;


std::vector<WStandardItem *> createRow(int lineNr, const std::string &line, const std::string &type, const std::string &event)
{
	std::vector<WStandardItem *> row;
	WStandardItem *col1 = new WStandardItem(boost::lexical_cast<std::string>(lineNr));
	WStandardItem *col2 = new WStandardItem(line);
	WStandardItem *col3 = new WStandardItem(type);
	WStandardItem *col4 = new WStandardItem(event);

	WString style;
	if (event == "full")
	{
		style = "covfull";
	}
	else if (event == "none")
	{
		style = "covnone";
	}
	else if (!event.empty())
	{
		style = "covelse";
	}

	if (!style.empty())
	{
		col2->setData(style, StyleClassRole);
	}

	row.push_back(col1);
	row.push_back(col2);
	row.push_back(col3);
	row.push_back(col4);
	return row;
}

CodeBrowserDlg::CodeBrowserDlg(WObject *parent) : Wt::WDialog(parent)
{
	setWidth(1024);
	setHeight(640);
	setWindowTitle("File: ????");
	setClosable(true);
	setResizable(true);

	m_lineModel = new WStandardItemModel(0, 4);
	WVBoxLayout *layout = new WVBoxLayout();
	m_tableView = new WTableView();

	m_tableView->setModel(m_lineModel);
	layout->addWidget(m_tableView);
	contents()->setLayout(layout);
}

CodeBrowserDlg::~CodeBrowserDlg() 
{


}

void CodeBrowserDlg::prepare(const std::string & fileName, const std::string & probeString)
{
	setWindowTitle(fileName);
	m_lineModel->clear();

	std::vector < std::string > tokens;
	boost::split(tokens, probeString, boost::is_any_of(";"));

	typedef std::pair<std::string, std::string> string_pair_t;
	std::map<int, string_pair_t> covMap;

    for (size_t i = 0; i < tokens.size() - 3; i += 3)
	{
		int lineNr = boost::lexical_cast<int>(tokens.at(i));
		covMap.insert(std::make_pair(lineNr, std::make_pair(tokens.at(i + 1), tokens.at(i + 2))));
	}


	std::string line;
    std::ifstream file(fileName.c_str());
	if (file.is_open())
	{
		int lineNr = 1;
		while (std::getline(file, line))
		{
			std::string type, event;

			if (covMap.count(lineNr))
			{
				type = covMap.find(lineNr)->second.first;
				event = covMap.find(lineNr)->second.second;
			}

			m_lineModel->appendRow(createRow(lineNr, line, type, event));
			lineNr++;
		}
		file.close();
	}

	m_lineModel->setHeaderData(0, Horizontal, std::string("Line"));
	m_lineModel->setHeaderData(1, Horizontal, std::string("Code"));
	m_lineModel->setHeaderData(2, Horizontal, std::string("Type"));
	m_lineModel->setHeaderData(3, Horizontal, std::string("Event"));

	m_tableView->setAlternatingRowColors(true);
	m_tableView->setColumnWidth(0, Wt::WLength(3, Wt::WLength::FontEm));
	m_tableView->setColumnWidth(1, Wt::WLength(60, Wt::WLength::FontEm));
	m_tableView->setColumnWidth(2, Wt::WLength(5, Wt::WLength::FontEm));
	m_tableView->setColumnWidth(3, Wt::WLength(5, Wt::WLength::FontEm));
}



